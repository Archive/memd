#include <gnome.h>
#include "gnomem.h"

GnomeApp *app;

int op_fd = -1;

static struct poptOption options [] = {
	{ "fd", '\0', POPT_ARG_INT, &op_fd, 0,
	  N_("File desciptor to talk to parent process"), N_("FD") },
	{ NULL, '\0', 0, NULL, 0 }
};

static GHashTable *alloc_hash;

static GtkWidget *l_allocs, *l_frees, *l_diffs;
static GtkWidget *l_alloced, *l_freed, *l_diff_size;
static long allocs, frees;
static long alloced, freed;

static void
register_malloc (int size, void *ptr)
{
	g_hash_table_insert (alloc_hash, ptr, GINT_TO_POINTER (size));
	allocs++;
	alloced += size;
}

static void
register_free (void *ptr)
{
	void *p;
	
	p = g_hash_table_lookup (alloc_hash, ptr);
	if (!p){
		printf ("Release of non-allocated memory %p\n");
		return;
	}
	g_hash_table_remove (alloc_hash, ptr);
	frees++;
	freed += GPOINTER_TO_INT(p);
}

static void
register_realloc (void *ptr, int size, void *newptr)
{
	void *p;
	
	p = g_hash_table_lookup (alloc_hash, ptr);
	if (!p){
		printf ("Realloc of unknown block %p\n", ptr);
		return;
	}
	g_hash_table_remove (alloc_hash, ptr);
	g_hash_table_insert (alloc_hash, newptr, GINT_TO_POINTER (size));
	/* adjust the allocated size */
	alloced += (size - GPOINTER_TO_INT(p));
}


static void
data_received (gpointer data, gint source, GdkInputCondition condition)
{
	GnomemOpType op;
	int size;
	void *ptr;
	void *newptr;
	
	read (source, &op, sizeof (op));
	switch (op){
	case GNOMEM_OP_MALLOC:
		read (source, &size, sizeof (size));
		read (source, &ptr, sizeof (ptr));
		register_malloc (size, ptr);
		break;
		
	case GNOMEM_OP_REALLOC:
		read (source, &ptr, sizeof (ptr));
		read (source, &size, sizeof (size));
		read (source, &ptr, sizeof (newptr));
		register_realloc (ptr, size, newptr);
		break;
		
	case GNOMEM_OP_FREE:
		read (source, &ptr, sizeof (ptr));
		register_free (ptr);
		break;

	default:
		printf ("Esto no debe de pasar: %d\n", op);
		g_error ("This should not happen!\n");
	}
}

static guint
ptr_hash (gconstpointer v)
{
	return GPOINTER_TO_INT (v);
}

static gint
ptr_comp (gconstpointer a, gconstpointer b)
{
	return a == b;
}

static void
init_alloc_track (void)
{
	alloc_hash = g_hash_table_new (ptr_hash, ptr_comp);
}

static void
table_add_label (GtkWidget *table, int row, char *text)
{
	GtkWidget *label = gtk_label_new (text);

	gtk_misc_set_alignment (GTK_MISC (label), 1.0, 0.5);
	gtk_table_attach (GTK_TABLE (table), label,
			  0, 1, row+1, row+2, GTK_FILL | GTK_EXPAND, 0, 5, 0);
}

static void
table_add_header (GtkWidget *table, int col, char *text)
{
	GtkWidget *label = gtk_label_new (text);

	gtk_table_attach (GTK_TABLE(table), label,
			  col+1, col+2, 0, 1, GTK_FILL | GTK_EXPAND, 0, 5, 0);
}

static GtkWidget *
table_add_label_res (GtkWidget *table, int row, int col)
{
	GtkWidget *label = gtk_label_new ("");

	gtk_table_attach (GTK_TABLE (table), label,
			  col+1, col+2, row+1, row+2, GTK_FILL | GTK_EXPAND,
			  0, 0, 0);

	return label;
}

static GtkWidget *
create_mem_info (void)
{
	GtkWidget *table = gtk_table_new (0, 0, 0);

	table_add_header (table, 0, "Num");
	table_add_header (table, 1, "Size");
	table_add_label (table, 0, "Allocs:");
	l_allocs = table_add_label_res (table, 0, 0);
	l_alloced = table_add_label_res (table, 0, 1);
	table_add_label (table, 1, "Frees:");
	l_frees = table_add_label_res (table, 1, 0);
	l_freed = table_add_label_res (table, 1, 1);
	table_add_label (table, 2, "Diff:");
	l_diffs = table_add_label_res (table, 2, 0);
	l_diff_size = table_add_label_res (table, 2, 1);
	return table;
}

static void
label_set (GtkWidget *w, long v)
{
	char buffer [40];

	sprintf (buffer, "%d", v);
	gtk_label_set_text (GTK_LABEL (w), buffer);
}

static gint
update_counters (gpointer data)
{
	label_set (l_allocs, allocs);
	label_set (l_frees,  frees);
	label_set (l_diffs,  allocs-frees);

	label_set (l_alloced,   alloced);
	label_set (l_freed,     freed);
	label_set (l_diff_size, alloced - freed);

	return TRUE;
}

int
main (int argc, char *argv [])
{
	poptContext ctx;

	g_message("LD_PRELOAD = %s", getenv("LD_PRELOAD"));
	
	gnome_init_with_popt_table ("GnoMem", "0.1", argc, argv, options, 0, NULL);

	app = (GnomeApp *) gnome_app_new ("GnoMem", "Gnome memory debugger");
	gtk_signal_connect(GTK_OBJECT(app), "destroy",
			   GTK_SIGNAL_FUNC(gtk_main_quit), NULL);
	gnome_app_set_contents (app, create_mem_info ());
	gtk_widget_show_all (GTK_WIDGET (app));

	allocs = frees = alloced = freed = 0;

	init_alloc_track ();
	gtk_timeout_add (500, update_counters, NULL);
	gdk_input_add (op_fd, GDK_INPUT_READ, data_received, NULL);
	
	gtk_main ();
}


