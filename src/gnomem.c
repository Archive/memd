#include <unistd.h>
#include <sys/types.h>
#include <sys/uio.h>
#include <errno.h>
#include "gnomem.h"

static int inited;
static int pipes [2];

void *__real_malloc  (int size);
void *__real_realloc (void *ptr, int size);
void *__real_calloc  (int nmemb, int size);
void *__real_strdup  (char *string);
void __real_free     (void *ptr);

static void
init_gnomem (void)
{
	pid_t pid;
	char buffer [20];
	
	inited = 1;
#ifdef LDPRELOAD_TARGET
	/* we don't want to memory debug gnomem-gui */
	unsetenv ("LD_PRELOAD");
#endif
	
	pipe (pipes);
	sprintf (buffer, "%d", pipes [0]);

	pid = fork ();
	inited = 1;
	if (pid == -1)
		_exit (1);

	if (pid == 0){
		execlp ("gnomem-gui", "gnomem-gui", "--fd", buffer, NULL);
		_exit (1);
	}
}

static void
register_malloc (int size, void *ptr)
{
	GnomemOpType op = GNOMEM_OP_MALLOC;
	int v;
	struct iovec iov [3] = {
		{ &op,   sizeof (op) },
		{ &size, sizeof (size) },
		{ &ptr,  sizeof (ptr) }
	};
	
	if (!inited)
		init_gnomem ();

	do {
		v = writev (pipes [1], iov, 3);
		if (v != -1)
			break;
	} while (errno == EINTR);
}

static void
register_free (void *ptr)
{
	GnomemOpType op = GNOMEM_OP_FREE;
	int v;
	struct iovec iov [2] = {
		{ &op,  sizeof (op) },
		{ &ptr, sizeof (ptr) },
	};
	
	if (!inited)
		init_gnomem ();

	do {
		v = writev (pipes [1], iov, 2);
		if (v != -1)
			break;
	} while (errno == EINTR);
}

void
register_realloc (void *ptr, int size, void *newptr)
{
	GnomemOpType op = GNOMEM_OP_REALLOC;
	int v;
	struct iovec iov [4] = {
		{ &op,     sizeof (op)   },
		{ &ptr,    sizeof (ptr)  },
		{ &size,   sizeof (size) },
		{ &newptr, sizeof (newptr) }
	};
	
	if (!inited)
		init_gnomem ();

	do {
		v = writev (pipes [1], iov, 4);
		if (v != -1)
			break;
	} while (errno == EINTR);
}

void *
__wrap_malloc (int size)
{
	void *v;

	v =  __real_malloc (size);

	register_malloc (size, v);
	
	return v;
}

void
__wrap_free (void *ptr)
{
	register_free (ptr);

	__real_free (ptr);
}

void *
__wrap_calloc (int nmemb, int size)
{
	void *v;
	
	v = __real_calloc (nmemb, size);

	register_malloc (nmemb*size, v);
	
	return v;
}

void *
__wrap_realloc (void *ptr, int size)
{
	void *v;

	v = __real_realloc (ptr, size);

	register_realloc (ptr, size, v);

	return v;
}

void *
__wrap_strdup (char *s)
{
	void *v;

	v = __real_strdup (s);
	if (v)
		register_malloc (strlen (s)+1, v);
	return v;
}
