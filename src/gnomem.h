#ifndef GNOMEM_H
#define GNOMEM_H

typedef enum {
	GNOMEM_OP_MALLOC,
	GNOMEM_OP_FREE,
	GNOMEM_OP_REALLOC
} GnomemOpType;

#endif
