/* these definitions alter the code in gnomem.c so that we have a library
 * that can be LD_PRELOAD'd so that applications do not need to be relinked
 * to use gnomem.
 * This quick hack probably won't work without glibc.  I will probably change
 * the __real_* macros into real functions with implementations like in
 * esddsp.
 */

#define LDPRELOAD_TARGET

#define __wrap_malloc  malloc
#define __wrap_calloc  calloc
#define __wrap_realloc realloc
#define __wrap_free    free
#define __wrap_strdup  strdup

#define __real_malloc  __malloc
#define __real_calloc  __calloc
#define __real_realloc __realloc
#define __real_free    __free
#define __real_strdup  __strdup

#include "gnomem.c"
